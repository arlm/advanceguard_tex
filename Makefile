#
#
#
NAME		:= AdvanceGuard
VERSION		:= 1.0
LATEX		:= pdflatex
LATEX_FLAGS	:= -interaction=nonstopmode	\
		   -file-line-error		\
		   -synctex 15			
CAIROFLAGS	:= -scale-to 800
EXPORT		:= $(HOME)/texmf/tex/latex/wargame/utils/export.py
MUTE		:= @
REDIR		:= > /dev/null 2>&1 
DOCFILES	:= $(NAME).tex			
SIGNATURE	:= 8
PAGES		:= 9-
PAPER		:= --a4paper
TARGETS		:= $(NAME).pdf 			\
		   $(NAME)Booklet.pdf		\
		   $(NAME)Materials.pdf		

TARGETSA4	:= $(NAME)A4.pdf 		\
		   $(NAME)A4Booklet.pdf 	

TARGETSLETTER	:= $(NAME)Letter.pdf 		\
		   $(NAME)LetterBooklet.pdf 	

ifdef VERBOSE	
MUTE		:=
REDIR		:=
LATEX_FLAGS	:= -synctex 15
endif

%.aux:	%.tex
	@echo "LATEX(1)	$*"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $* $(REDIR)

%.pdf:	%.aux
	@echo "LATEX(2)	$*"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $* $(REDIR)

%A4.aux:%.tex
	@echo "LATEX(1)	$*A4"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A4 $* $(REDIR)

%A4.pdf:%A4.aux
	@echo "LATEX(2)	$*A4"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A4 $* $(REDIR)

%Letter.aux:	%.tex
	@echo "LATEX(1)	$*Letter"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Letter $* $(REDIR)

%Letter.pdf:	%Letter.aux
	@echo "LATEX(2)	$*Letter"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Letter $* $(REDIR)


%Booklet.pdf:%.pdf
	@echo "BOOKLET		$*"
	$(MUTE)pdfjam 				\
		--landscape 			\
		--suffix book 			\
		--signature '$(SIGNATURE)' 	\
		$(PAPER) 			\
		--no-tidy 			\
		--outfile $@ 			\
		-- $< '1-$(SIGNATURE)' $(REDIR)

%Materials.pdf:%.pdf
	@echo "MATERIALS	$@"
	$(MUTE)pdfjam 				\
		--suffix materials		\
		$(PAPER) 			\
		--no-tidy 			\
		--outfile $@ 			\
		-- $< '$(PAGES)' $(REDIR)

%.vmod:%.pdf %Export.pdf %.py
	$(EXPORT) $*Export.pdf $*Export.csv -r $< -o $@ \
	-t $* -v $(VERSION) -p $*.py

%.png:%.pdf
	pdftocairo $(CAIROFLAGS) -singlefile -png $< $*

%.png:%A4.pdf
	pdftocairo $(CAIROFLAGS) -singlefile -png $< $*

all:	$(TARGETS)
a4:	$(TARGETSA4)
us:	$(TARGETSLETTER)
letter:	us
vmods:	Tannenberg4.vmod 

distdir:$(TARGETS) README.md
	@echo "DISTDIR	$(NAME)-$(VERSION)"
	$(MUTE)mkdir -p $(NAME)-$(VERSION)
	$(MUTE)cp $^ $(NAME)-$(VERSION)/
	$(MUTE)touch $(NAME)-$(VERSION)/*

dist:	distdir
	@echo "DIST	$(NAME)-$(VERSION).zip"
	$(MUTE)zip -r $(NAME)-$(VERSION).zip $(NAME)-$(VERSION)
	$(MUTE)rm -rf $(NAME)-$(VERSION)

distdirA4:$(TARGETSA4) README.md
	@echo "DISTDIR	$(NAME)-A4-$(VERSION)"
	$(MUTE)mkdir -p $(NAME)-A4-$(VERSION)
	$(MUTE)cp $^ $(NAME)-A4-$(VERSION)/
	$(MUTE)touch $(NAME)-A4-$(VERSION)/*

distA4:	distdirA4
	@echo "DIST	$(NAME)-A4-$(VERSION).zip"
	$(MUTE)zip -r $(NAME)-A4-$(VERSION).zip $(NAME)-A4-$(VERSION)
	$(MUTE)rm -rf $(NAME)-A4-$(VERSION)

distdirLetter:$(TARGETSLETTER) README.md
	@echo "DISTDIR	$(NAME)-Letter-$(VERSION)"
	$(MUTE)mkdir -p $(NAME)-Letter-$(VERSION)
	$(MUTE)cp $^ $(NAME)-Letter-$(VERSION)/
	$(MUTE)touch $(NAME)-Letter-$(VERSION)/*

distLetter:	distdirLetter
	@echo "DIST	$(NAME)-Letter-$(VERSION).zip"
	$(MUTE)zip -r $(NAME)-Letter-$(VERSION).zip $(NAME)-Letter-$(VERSION)
	$(MUTE)rm -rf $(NAME)-Letter-$(VERSION)

clean:
	@echo "CLEAN"
	$(MUTE)rm -f *~ *.log *.aux *.out *.lot *.lof *.toc *.auxlock
	$(MUTE)rm -f *.synctex* *.pdf *-pdfjam.pdf
	$(MUTE)rm -f $(TARGETS) $(TARGETSA4) $(TARGETSLETTER) 
	$(MUTE)rm -f *.vmod *.csv
	$(MUTE)rm -rf __pycache__

realclean: clean
	@echo "CLEAN ALL"
	$(MUTE)rm -rf cache/*  labels*.tex
	$(MUTE)rm -rf $(NAME)-*-$(VERSION)

distclean:realclean
	@echo "CLEAN DIST"
	$(MUTE)rm -rf *.zip
	$(MUTE)rm -rf $(NAME)-$(VERSION)
	$(MUTE)rm -rf $(NAME)-Letter-$(VERSION)
	$(MUTE)rm -rf $(NAME)-A4-$(VERSION)

$(NAME).aux:		$(NAME).tex
$(NAME)A4.aux:		$(NAME).tex 	
$(NAME)Letter.aux:	$(NAME).tex	

docker-artifacts: distdirA4 distdirLetter

Tannenberg4Booklet.pdf:		SIGNATURE:=2
Tannenberg4Materials.pdf:	PAGES:=6-

#
# EOF
#

