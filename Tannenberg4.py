# --------------------------------------------------------------------
# some utilities 
def print_tree(doc,ind=''):
    print(f'{ind}{doc}')
    for c in doc.childNodes:
        print_tree(c,ind+' ')

def print_attr(doc):
    from pprint import pprint

    pprint(doc.attributes.items())
    
# --------------------------------------------------------------------
# Our main patching function
def patch(build,data,vmod,verbose=False):
    from pprint import pprint
    import export

    # ----------------------------------------------------------------
    # Get all boards 
    maps = export.get_maps(build)
    mainMap = maps['Board']

    # ----------------------------------------------------------------
    # Get all boards 
    boards = export.get_boards(build)

    # ----------------------------------------------------------------
    # The main board.  We will work on this first 
    mainBoard = boards['Board']

    # ----------------------------------------------------------------
    # Adjust the hexes
    #
    # Get the hex grid and turn of drawing 
    hexgrid   = export.get_hexgrid(mainBoard,one=True)[0]
    hexgrid.setAttribute('visible','false')
    # Numbering - We turn off drawin and set the starting row number 
    hexgrid_number = export.get_hexnumbering(hexgrid,one=True)[0]
    hexgrid_number.setAttribute('vOff','-4') # Ascending rows
    hexgrid_number.setAttribute('visible','false')

    # ----------------------------------------------------------------
    # Add zone for turn track 
    #
    # First, get the zoned area 
    zoned = export.get_zoned(mainBoard,one=True)[0]
    # We must add our Turn zone _before_ the hex zone, so first we get
    # that zone
    full = export.get_zone(zoned)['full']
    # Now we remove that child from zoned
    zoned.removeChild(full)
    # Then add a zone (we can use the debug window to read off coordinates)
    zone = export.add_zone(build,zoned,'Turn',
                           path='79,707;911,707;911,788;79,788',
                           useHighlight=True)
    grid = export.add_squaregrid(build,zone,
                                 color=export.rgb(255,0,0),
                                 x0=-43,
                                 y0=0,
                                 dx=82.8,
                                 dy=82.8,
                                 visible=False)
    export.add_squarenumbering(build,grid,
                               sep='T',
                               hOff=-1,
                               vOff=8,
                               visible=False)
    # We add back in the full zone
    zoned.appendChild(full)

    # ----------------------------------------------------------------
    # Add starting units
    #
    # Get all the counter
    counters = export.get_pieces(build)

    # Add at start stacks
    for u,h in [['german 8', 'D5'],
                ['russian 1', 'I6'],
                ['russian 2', 'D2']]:
        export.add_atstart(build,mainMap, h,
                           counters[u],
                           owningBoard='Board',
                           location = h,
                           useGridLocation = True)
    export.add_atstart(build,mainMap, 'Turn 1',
                       counters['game turn'],
                       owningBoard='Board',
                       location = '01T0',
                       x = 123,
                       y = 748,
                       useGridLocation = True)

    if verbose:
        print('Done patching')
