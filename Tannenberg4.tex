%%
%% Document class and font size
%%
%% 11pt is a good size for full page text and for booklets
%%
\documentclass[11pt]{article}
%%
%% Collection of our settings are in a separate package
%%
\usepackage{tannenberg}
%%
%% Paper size and margins
%%
\usepackage[a4paper,margin=1cm,bottom=2cm]{geometry}
  
%%
%% Start of the document 
%%  
\begin{document}
%%
%% We want to make the document two column, but the title should be in
%% single column mode
\twocolumn[%
\begin{center}
  {\Huge\textbf{Tannenberg 4}}
  
  {\Large\textsl{Pelle Nilsson}}

  {\href{https://boardgamegeek.com/boardgame/42080/tannenberg-4}{from BoardGameGeek}}
\end{center}]
%%
%% First, the rules
%%
\section*{Introduction}

The Battle of Tannenberg was a conflict between the Empires of Germany
and Russia in East Prussia in late August 1914.  One player controls
the attacking Russian faction, and the other player controls the
defending German faction.  

\section{Components}

\begin{itemize}
\item One Hex-map
\item Three unit counters 
\item Turn counter
\item One six-sided dice
\end{itemize}

\section{The board}

Central to the board is a map of East Prussia near Konigsberg. On the
map are hexagonal fields (hexes) which capture the terrain and
manoeuvres of the troops involved in the battle.  The hexes are
interconnected by railroad lines, which provides the only means of the
troops to move around the map.  Hexes are label by their grid
coordinates.  The city of Konigsberg is at D6, and is illustrated by a
fortification.  Three other hexes, D5, D2, and I6, are marked as
set-up hexes for the German and Russian factions.

Below the map is a turn track to keep track of time.  Below that, to
the right, is the \emph{Combat Resolution Table} (CRT) which is used
to resolve battles between the factions. 

\section{Counters}

\begin{figure}
  \centering
  %% Below is a picture of the four counters in the game.  We annotate
  %% the various elements of the counters.
  \begin{tikzpicture}[oval/.style={shape=circle,draw=blue,line
      width=1pt},
    line/.style={<-,blue},
    annot/.style={inner sep=.1mm,blue}]
    \node[german 8]  at (-2,0) {};   
    \node[russian 1] at ( 0,0) {};   
    \node[russian 2] at ( 2,0) {};   
    \node[chit={full=game turn}] at ( 4,0) {};

    \node[oval,minimum size=.2] (cf) at (-2.25,-.38) {};
    \node[oval,minimum size=.2] (mf) at (0.25,-.38) {};
    \node[oval,minimum size=.2] (id) at (2.45,+.23) {};
    \node[oval,minimum size=.4] (ec) at (0   ,+.5) {};
    \node[oval,minimum size=.4] (st) at (-2.4 ,+.5) {};
    \draw[line] (cf) --++(-120:.5) node[annot,anchor=north east]{%
      \begin{tabular}{@{}r@{}}Combat\\factor\end{tabular}};
    \draw[line] (mf) --++(-60:.5) node[annot,anchor=north west]{%
      \begin{tabular}{@{}l@{}}Movement\\factor\end{tabular}};
    \draw[line] (st) --++(120:.5) node[annot,anchor=south east]{Setup};
    \draw[line] (ec) --++(60:.5) node[annot,anchor=south west]{Echelon};
    \draw[line] (id) --++(60:.8) node[annot,anchor=south west]{Identifier};
    \draw[line] (2,.2) --++(-60:1.25) node[annot,anchor=north west]{Type};
  \end{tikzpicture}
  \caption{Units and turn counter}
  \label{fig:units}
\end{figure}

The Russian faction controls the two units representing the Russian
1\textsuperscript{st} and 2\textsuperscript{nd} armies, and the German 
faction controls the third counter, representing the German
8\textsuperscript{th} army, shown in Figure~\ref{fig:units}.
1
Each unit has a \emph{combat} and \emph{movement} factor, as shown in
the figure.  All counters have \emph{infantry} type, and all are on
the \emph{army} echelon.  The unit identifier is mostly for historical
reference.

Units can only occupy \emph{one} hex on the board, and only hexes with
coordinates in them.   No two units may occupy the same hex, and units
are adjacent if they occupy adjacent hexes. 

\section{Setup}

Place the three units in the hex indicated in the top left corner of
the unit. 

\section{Turn sequence}

The battle is simulated over 10 turns, or until all units of a faction
is eliminated, what ever comes first.  Each turn follows the same turn
sequence shown in Table~\ref{tab:turnsequence}.

\begin{table}
  \centering
  \turnsequence
  \caption{Turn sequence}
  \label{tab:turnsequence}
\end{table}

\section{Movement}

Movement is \emph{always} optional.  A unit can \emph{only} move along
railroads.  That is, to move a unit into over a hex side, that hex
side must be intersected by a railroad.

\begin{quote}
  \itshape This means that some hex sides on the map cannot be
  traversed during movement.  For example, a unit cannot move from E6
  to E5 in a turn.  Instead, it must first move to either F6 or D6,
  and then, in the next turn, to E5.
\end{quote}

A unit cannot be moved into a hex occupied by another unit, whether or
not that unit is friendly or not.  A unit adjacent to an enemy unit
\emph{cannot} be moved into another hex also adjacent to an enemy
unit.

See also Figure~\ref{fig:move} for an illustration of these rules.

\begin{figure}
  %%
  %% This is an example of how to draw an example using the defined
  %% map and units.  We use a part of the map, defined by the `\clip'
  %% below.  Then, we draw some moves using the style `hex/short move'
  %% in one group using dark red, and in the other using green.
  %% The units are placed on the map via the `\node' lines using the
  %% previously defined styles.
  %% 
  \centering
  \begin{tikzpicture}
    \begin{scope}
      \clip (hex cs:c=3,r=4) rectangle (hex cs:c=8,r=7);
      \hexes
      \begin{scope}[m/.style={hex/move=red!75!black},
        font=\Large,text=red!75!black]
        \draw[m] (hex cs:c=6,r=6) -- (hex cs:c=6,r=5) node{\ding{56}};
        \draw[m] (hex cs:c=6,r=6) -- (hex cs:c=7,r=6) node{\ding{56}};
        \draw[m] (hex cs:c=6,r=6) -- (hex cs:c=5,r=5) node{\ding{56}};
      \end{scope}
      \begin{scope}[m/.style={hex/move=green!75!black},
        font=\Large,text=green!75!black]
        \draw[m] (hex cs:c=6,r=6) -- (hex cs:c=5,r=6) node{\ding{52}};
      \end{scope}
      \node[german 8] (g8) at (hex cs:c=6,r=6) {};
      \node[russian 1] at (hex cs:c=7,r=5) {};
      \node[russian 2] at (hex cs:c=4,r=5) {};
    \end{scope}
  \end{tikzpicture}
  \caption{The German 8\textsuperscript{th} army is in F6, adjacent to
    the Russian 1\textsuperscript{st} army in G5, while the Russian
    2\textsuperscript{nd} army is in D5.  The German unit
    \emph{cannot} move into G5 since it is already occupied.
    \emph{Nor} can the German unit move to E5, F5 or G6, since those
    hexes are adjacent to an enemy unit.  It \emph{can} move to E6.
    The German faction of course has the option to \emph{not} move the
    unit at all.}
  \label{fig:move}
\end{figure}

\tikz{\pic[scale=.5]{russian flag}} \emph{Russian faction only}:
During odd numbered turns, the Russian faction may \emph{at most} move
\emph{one} unit.  During even numbered turns, the Russian faction
\emph{may} move \emph{both} Russian units.

\section{Combat}

A unit adjacent to an enemy unit \emph{may} attack the enemy unit in
the factions combat phase. 

\tikz{\pic[scale=.5]{russian flag}} \emph{Russian faction only}: If
both Russian units are adjacent to the German unit, then they may
combine forces and \emph{both} attack the German unit in a
\emph{single} combat with a combined combat factor of 4.

\tikz{\pic[scale=.5]{german flag}} \emph{German faction only}: If the
German unit is adjacent to \emph{both} Russian units, then it may
attack \emph{either} of them.  That is, the German faction can attack
\emph{one}, and only one, Russian unit in a turn. 

To resolve the combat, determine the \emph{combat odds} by comparing
the attackers combat factor to the defenders combat factor.  Then roll
a die, and cross-index the combat odds with the die roll in the
\emph{combat resolution table} shown in Table~\ref{tab:crt} and on the
board.

\begin{table}
  \centering
  \crt
  \caption{Combat resolution table. Compare attackers combat factor to
    defenders combat factor for the combat odds, and roll a die.}
  \label{tab:crt}
\end{table}

The results are
\begin{description}
\item[---] No effect. 
\item[AE] Attacker eliminated.  One attacking unit is eliminated. 
\item[DR] Defender retreat.  The defending units \emph{must} retreat
  one hex, by the defending faction.  The hex retreated into
  \emph{must} have a railroad in it, but \emph{need not} be connected
  by railroad to the hex that the defending unit previously occupied.
  This is the \emph{only} case a unit may move outside of railroads.
\item[DE] Defender eliminated. One defending unit is eliminated. 
\end{description}

See also Figure~\ref{fig:combat} for illustrations of these rules.

\begin{figure}
  \centering
  \begin{tikzpicture}
    \begin{scope}
      \clip (hex cs:c=3,r=1) rectangle (hex cs:c=6,r=4);
      \hexes
      \node[german 8] (g8) at (hex cs:c=5,r=3) {};
      \node[russian 2] (r2) at (hex cs:c=5,r=2) {};
      \shadechit(hex cs:c=5,r=2); % Semi-transparent overlay
      \node[russian 2] (r2r) at (hex cs:c=4,r=3) {};
      \draw[hex/attack] (g8)--(r2); % Attack arrow
      \draw[hex/retreat] (r2)--(r2r); % Retreat arrow
    \end{scope}
  \end{tikzpicture}
  \begin{tikzpicture}
    \begin{scope}
      \clip (hex cs:c=3,r=4) rectangle (hex cs:c=6,r=7);
      \hexes
      \node[german 8] (g8) at (hex cs:c=4,r=6) {};
      \shadechit(hex cs:c=4,r=6); % Semi-transparent overlay
      \node[german 8] (g8r) at (hex cs:c=5,r=6) {};
      \node[russian 2] (r2) at (hex cs:c=4,r=5) {};
      \node[russian 1] (r1) at (hex cs:c=5,r=5) {};
      \draw[hex/attack] (r1)--(g8);
      \draw[hex/attack] (r2)--(g8);
      \draw[hex/retreat] (g8)--(g8r);
    \end{scope}
  \end{tikzpicture}
  \caption{Left: The German 8\textsuperscript{th} army in E3 attacks
    the Russian 2\textsuperscript{nd} army in E2 at odds 3:2.  The
    result is DR.  The Russian faction decides to retreat into
    D3. Right: Both Russian armies attack the German army at odds 4:3,
    with DR result.  The German faction retreats the unit to E6 in the
    hope of knocking off the Russian 1\textsuperscript{nd} army in its
    turn, or at least prevent it from moving into Konigsberg. Note
    that the Russian 2\textsuperscript{nd} army may still move into
    Konigsberg on the next Russian movement phase. }
  \label{fig:combat}
\end{figure}

\section{Victory}

If a faction eliminates all opposing units, it wins
\emph{immediately}.

If the Russian faction occupies Konigsberg (D6) at the end of the
10\textsuperscript{th} turn, then the Russian faction wins.
Otherwise, the German faction wins.

%%
%% Below is a replay of the game
%%
\appendix
\twocolumn[\begin{center}{\LARGE\textbf{Replay}}\end{center}]
\section*{Turn 1}
\begin{tikzpicture}[
  scale=.5,
  transform shape,
  gm/.style={hex/move=german!50!black},
  rm/.style={hex/move=russian!50!black}
  ]
  \hexes
  \coordinate (g1)  at (hex cs:c=4,r=5);
  \coordinate (g2)  at (hex cs:c=5,r=4);
  \coordinate (r11) at (hex cs:c=9,r=6);
  \coordinate (r12) at (hex cs:c=8,r=6);
  \coordinate (r21) at (hex cs:c=4,r=2);
  \coordinate (r22) at (hex cs:c=4,r=2);
  \draw[gm] (g1)--(g2);
  \node[german 8]  at (g1){};\shadechit(g1); \node[german 8]  at (g2){};
  \draw[rm] (r11) -- (r12);
  \node[russian 1]  at (r11){};\shadechit(r11); \node[russian 1]  at (r12){};
  \node[russian 2]  at (r21){};%\shadechit(r21); \node[russian 2]  at (r22){};
\end{tikzpicture}

German 8\textsuperscript{th} army move to E4, and Russian
1\textsuperscript{st} move to H6.

\section*{Turn 2}
\begin{tikzpicture}[
  scale=.5,
  transform shape,
  gm/.style={hex/move=german!50!black},
  rm/.style={hex/move=russian!50!black}
  ]
  \hexes
  \coordinate (g1)  at (hex cs:c=5,r=4);
  \coordinate (g2)  at (hex cs:c=5,r=3);
  \coordinate (r11) at (hex cs:c=8,r=6);
  \coordinate (r12) at (hex cs:c=7,r=5);
  \coordinate (r21) at (hex cs:c=4,r=2);
  \coordinate (r22) at (hex cs:c=3,r=2);
  \draw[gm] (g1)--(g2);
  \node[german 8]  at (g1){};\shadechit(g1); \node[german 8]  at (g2){};
  \draw[rm] (r11) -- (r12);
  \draw[rm] (r21) -- (r22);
  \node[russian 1]  at (r11){};\shadechit(r11); \node[russian 1]  at (r12){};
  \node[russian 2]  at (r21){};\shadechit(r21); \node[russian 2]  at (r22){};
\end{tikzpicture}

German 8\textsuperscript{th} army move to E3, hoping that the Russian
faction will move the 1\textsuperscript{st} army to E2 and risk the
attack (1 in 6 change of eliminating the Russian army).  However, the
Russian faction does not fall for the trap, and move the
1\textsuperscript{st} army to C2, and the 2\textsuperscript{nd} army
to G5.  

\section*{Turn 3}
\begin{tikzpicture}[
  scale=.5,
  transform shape,
  gm/.style={hex/move=german!50!black},
  rm/.style={hex/move=russian!50!black}
  ]
  \hexes
  \coordinate (g1)  at (hex cs:c=5,r=3);
  \coordinate (g2)  at (hex cs:c=5,r=4);
  \coordinate (r11) at (hex cs:c=7,r=5);
  \coordinate (r12) at (hex cs:c=6,r=6);
  \coordinate (r21) at (hex cs:c=3,r=2);
  \coordinate (r22) at (hex cs:c=3,r=2);
  \draw[gm] (g1)--(g2);
  \node[german 8]  at (g1){};\shadechit(g1); \node[german 8]  at (g2){};
  \draw[rm] (r11) -- (r12);
  \node[russian 1]  at (r11){};\shadechit(r11); \node[russian 1]  at (r12){};
  \node[russian 2]  at (r21){};%\shadechit(r11); \node[russian 2]  at (r12){};
\end{tikzpicture}

Since the Russian faction did not fall for the trap, the German
faction moves the 8\textsuperscript{th} army back to E4 to keep the
Russian 1\textsuperscript{st} army get closer to Konigsberg.  The 
Russian faction does not want to risk an attack by moving the
1\textsuperscript{st} army to F5, but closes in on Konigsberg by
moving to F6. 

\section*{Turn 4}
\begin{tikzpicture}[
  scale=.5,
  transform shape,
  gm/.style={hex/move=german!50!black},
  rm/.style={hex/move=russian!50!black}
  ]
  \hexes
  \coordinate (g1)  at (hex cs:c=5,r=4);
  \coordinate (g2)  at (hex cs:c=6,r=5);
  \coordinate (r11) at (hex cs:c=6,r=6);
  \coordinate (r12) at (hex cs:c=7,r=6);
  \coordinate (r21) at (hex cs:c=3,r=2);
  \coordinate (r22) at (hex cs:c=4,r=2);
  \draw[gm] (g1)--(g2);
  \node[german 8]  at (g1){};\shadechit(g1) \node[german 8]  at (g2){};
  \draw[rm] (r21) -- (r22);
  \node[russian 1]  at (r11){}; \shadechit(r11) \node[russian 1]  at (r12){};
  \node[russian 2]  at (r21){}; \shadechit(r21) \node[russian 2]  at (r22){};
  \draw[hex/attack]  (g2)--(r11);
  \draw[hex/retreat] (r11)--(r12);
\end{tikzpicture}

The German 8\textsuperscript{th} army move in (F5) for the attack
against the Russian 1\textsuperscript{st} army in F6 and obtains a DR
result.  The Russian faction retreats the 1\textsuperscript{st} army
to G6.  In the mean time, the Russian faction moves the
2\textsuperscript{nd} army to D2.

\section*{Turn 5}
\begin{tikzpicture}[
  scale=.5,
  transform shape,
  gm/.style={hex/move=german!50!black},
  rm/.style={hex/move=russian!50!black}
  ]
  \hexes
  \coordinate (g1)  at (hex cs:c=6,r=5);
  \coordinate (g2)  at (hex cs:c=6,r=6);
  \coordinate (r11) at (hex cs:c=7,r=6);
  %\coordinate (r12) at (hex cs:c=7,r=6);
  \coordinate (r21) at (hex cs:c=4,r=2);
  \coordinate (r22) at (hex cs:c=5,r=2);
  \draw[gm] (g1)--(g2);
  \node[german 8]  at (g1){};\shadechit(g1)
  \node[german 8]  at (g2){};
  \draw[rm] (r21) -- (r22);
  \node[russian 1]  at (r11){}; % \shadechit(r11) \node[russian 1]  at (r12){};
  \node[russian 2]  at (r21){};\shadechit(r21); \node[russian 2]  at (r22){};
  \draw[hex/attack]  (g2)--(r11);
  \draw[hex/attack]  (r11)--(g2);
\end{tikzpicture}

The German faction decides to press on, and moves the
8\textsuperscript{th} army move to F6, and attack 
the Russian 1\textsuperscript{st} army again.  The result is ---.  In
the Russian movement phase the 2\textsuperscript{nd} army moves
further north, while the 1\textsuperscript{st} army, holds its ground,
only to counterattack against the German 8\textsuperscript{th} army (a
risky move, since there is a 1 in 6 change of eliminating the Russian
unit).  The result is again ---.

\section*{Turn 6}
\begin{tikzpicture}[
  scale=.5,
  transform shape,
  gm/.style={hex/move=german!50!black},
  rm/.style={hex/move=russian!50!black}
  ]
  \hexes
  \coordinate (g1)  at (hex cs:c=6,r=6);
  \coordinate (g2)  at (hex cs:c=6,r=6);
  \coordinate (r11) at (hex cs:c=7,r=6);
  \coordinate (r12) at (hex cs:c=7,r=5);
  \coordinate (r21) at (hex cs:c=5,r=2);
  \coordinate (r22) at (hex cs:c=5,r=3);
  \draw[gm] (g1)--(g2);
  \node[german 8]  at (g1){}; % \shadechit(g1) \node[german 8]  at (g2){};
  \draw[rm] (r21) -- (r22);
  \node[russian 1]  at (r11){};\shadechit(r11) \node[russian 1]  at (r12){};
  \node[russian 2]  at (r21){};\shadechit(r21) \node[russian 2]  at (r22){};
  \draw[hex/attack]  (g2)--(r11);
  \draw[hex/retreat] (r11)--(r12);
\end{tikzpicture}

The German 8\textsuperscript{th} army attacks the Russian
1\textsuperscript{st} army again, but this time the result is DR. The
Russian faction retreats to G5 this time, as the 2\textsuperscript{nd}
army is moving in.  Again the Russian 2\textsuperscript{nd} army moves
further north, while the 1\textsuperscript{st} army, holds its ground
and chooses not to counterattack. 

\section*{Turn 7}
\begin{tikzpicture}[
  scale=.5,
  transform shape,
  gm/.style={hex/move=german!50!black},
  rm/.style={hex/move=russian!50!black}
  ]
  \hexes
  \coordinate (g1)  at (hex cs:c=6,r=6);
  \coordinate (g2)  at (hex cs:c=5,r=5);
  \coordinate (r11) at (hex cs:c=7,r=5);
  \coordinate (r12) at (hex cs:c=7,r=5);
  \coordinate (r21) at (hex cs:c=5,r=3);
  \coordinate (r22) at (hex cs:c=4,r=4);
  \draw[gm] (g1)--(g2);
  \node[german 8]  at (g1){};\shadechit(g1); \node[german 8]  at (g2){};
  \draw[rm] (r21) -- (r22);
  \node[russian 1]  at (r11){}; % \shadechit(r11)\node[russian 1]  at (r11){};
  \node[russian 2]  at (r21){};\shadechit(r21) \node[russian 2]  at (r22){};
\end{tikzpicture}

There is a serious risk that the the German 8\textsuperscript{th} army
will be out flanked by the Russian 2\textsuperscript{nd} army again,
and the German faction decides to take up a more defensive position in
E5.  The Russian 2\textsuperscript{nd} army moves to D4 to try to
ensnare the German unit. 

\section*{Turn 8}
\begin{tikzpicture}[
  scale=.5,
  transform shape,
  gm/.style={hex/move=german!50!black},
  rm/.style={hex/move=russian!50!black}
  ]
  \hexes
  \coordinate (g1)  at (hex cs:c=5,r=5);
  \coordinate (g2)  at (hex cs:c=4,r=5);
  \coordinate (r11) at (hex cs:c=7,r=5);
  \coordinate (r12) at (hex cs:c=6,r=6);
  \coordinate (r21) at (hex cs:c=4,r=4);
  \coordinate (r22) at (hex cs:c=5,r=4);
  \draw[gm] (g1)--(g2);
  \node[german 8]  at (g1){};\shadechit(g1); \node[german 8]  at (g2){};
  \draw[rm] (r11) -- (r12);
  \node[russian 1]  at (r11){};\shadechit(r11)\node[russian 1]  at (r11){};
  \node[russian 2]  at (r21){};\shadechit(r21) \node[russian 2]  at (r22){};
  \draw[hex/attack] (g2)--(r21);
  \draw[hex/retreat] (r21)--(r22);
\end{tikzpicture}

The German faction is in a bit of a pickle.  If the
8\textsuperscript{th} army stays at E5, then both Russian armies can
move in to attack it.  However, if the German unit moves toward either
of the two Russian units, then the other Russian unit may move in to
cut it off from further manoeuvres.   The German faction decides to
move to D5 and attack the Russian 2\textsuperscript{nd} army, in the
hope of displacing it.  The result of the combat is DR, and the
Russian faction retreats 2\textsuperscript{nd} army to E4.  The
Russian 2\textsuperscript{nd} army cannot move in a meaningful way, so
all the Russian faction does is to move the 1\textsuperscript{st} army
to F6. 

\section*{Turn 9}
\begin{tikzpicture}[
  scale=.5,
  transform shape,
  gm/.style={hex/move=german!50!black},
  rm/.style={hex/move=russian!50!black}
  ]
  \hexes
  \coordinate (g1)  at (hex cs:c=4,r=5);
  \coordinate (g2)  at (hex cs:c=3,r=5);
  \coordinate (r11) at (hex cs:c=6,r=6);
  \coordinate (r12) at (hex cs:c=5,r=6);
  \coordinate (r21) at (hex cs:c=5,r=4);
  \coordinate (r22) at (hex cs:c=5,r=4);
  \draw[gm] (g1)--(g2);
  \node[german 8]  at (g1){};\shadechit(g1); \node[german 8]  at (g2){};
  \draw[rm] (r11) -- (r12);
  \node[russian 1]  at (r11){};\shadechit(r11)\node[russian 1]  at (r12){};
  \node[russian 2]  at (r21){};%\shadechit(r21) \node[russian 2]  at (r22){};
  %\draw[hex/attack] (g2)--(r21);
  %\draw[hex/retreat] (r21)--(r22);
\end{tikzpicture}

This is the crucial part for the German faction.  It must try to
displace the Russian 2\textsuperscript{nd} army or face both Russian
armies during the Russian combat phase, or it can retreat to C5, which
however will only postpone that combat.  However, if the Germans only
achieve a DR result, then the Russian 2\textsuperscript{nd} army may
retreat to E5, which will almost certainly assure a Russian victory.
No matter what the outcome of the combat, the Russian
1\textsuperscript{st} army can move to E6, and the German army cannot
interdict it before it reaches Konigsberg.  Thus, the German faction
opts for the strategic retreat to C5.  The Russian
1\textsuperscript{st} army moves to E5, not realising that it has
already lost the battle. 

\section*{Turn 10}
\begin{tikzpicture}[
  scale=.5,
  transform shape,
  gm/.style={hex/move=german!50!black},
  rm/.style={hex/move=russian!50!black}
  ]
  \hexes
  \coordinate (g1)  at (hex cs:c=3,r=5);
  \coordinate (g2)  at (hex cs:c=4,r=6);
  \coordinate (r11) at (hex cs:c=5,r=6);
  \coordinate (r12) at (hex cs:c=5,r=6);
  \coordinate (r21) at (hex cs:c=5,r=4);
  \coordinate (r22) at (hex cs:c=4,r=5);
  \draw[gm] (g1)--(g2);
  \node[german 8]  at (g1){};\shadechit(g1); \node[german 8]  at (g2){};
  \shadechit(g2); \node[german 8]  at ($(g1)+(-.3,.3)$){};
  \draw[rm] (r21) -- (r22);
  \node[russian 1]  at (r11){};\shadechit(r11)\node[russian 1]  at (r11){};
  \node[russian 2]  at (r21){};\shadechit(r21) \node[russian 2]  at (r22){};
  \draw[hex/attack] (r11)--(g2);
  \draw[hex/attack] (r22)--(g2);
  \draw[hex/retreat] (g2)--(g1);
\end{tikzpicture}

The German 8\textsuperscript{th} army moves into Konigsberg to take up
an defensive position, and attacks the Russian 1\textsuperscript{st}
army for a result of ---.  The Russian 2\textsuperscript{nd} army
moves to D5 to surround Konigsberg.  Both Russian armies attack
Konigsberg for a result of DR, and the German unit retreats back to
C5. However, it is too late for the Russians.  Since there are no more
turns in the game, it cannot move into Konigsberg, and thus loses the
game. 


%% The rest of the document is in single column to leave room for the
%% full board 
\onecolumn
\begin{tikzpicture}
  \board
\end{tikzpicture}

\begin{tikzpicture}[chit/cell background/.style={fill=white,draw=none}]
  \draw[dashed](-.8,  .6)--++(2.9,0);
  \draw[dashed](-.8,- .6)--++(2.9,0);
  \draw[dashed](-.8,- .7)--++(2.9,0);
  \draw[dashed](-.8,-1.9)--++(2.9,0);
  \draw[dashed](-.6,.8)--++(0,-2.9);
  \draw[dashed]( .6,.8)--++(0,-2.9);
  \draw[dashed]( .7,.8)--++(0,-2.9);
  \draw[dashed](1.9,.8)--++(0,-2.9);
  \chits{{russian 1,russian 2, german 8, game turn}}{2}{1.3}
\end{tikzpicture}


\end{document}
